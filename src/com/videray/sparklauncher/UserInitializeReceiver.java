/*
 *  (C) Copyright 2018-2019 Videray Technologies, Inc. (https://videray.com)
 *  All rights reserved.
 *
 */

package com.videray.sparklauncher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class UserInitializeReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(Context context, Intent intent) {}
}
