/*
 *  (C) Copyright 2018-2019 Videray Technologies, Inc. (https://videray.com)
 *  All rights reserved.
 *
 */

package com.videray.sparklauncher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class PackageChangedReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(Context context, Intent intent) {
    Log.i("PackageChangedReceiver", "onReceive " + intent);

    final String packageName = intent.getData().getSchemeSpecificPart();

    if (packageName == null || packageName.length() == 0) {
      // they sent us a bad intent
      return;
    }
    LauncherApplication app = (LauncherApplication) context.getApplicationContext();
    app.refreshApps();
  }
}
