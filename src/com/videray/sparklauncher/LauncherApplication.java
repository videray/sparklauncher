/*
 *  (C) Copyright 2018-2019 Videray Technologies, Inc. (https://videray.com)
 *  All rights reserved.
 *
 */

package com.videray.sparklauncher;

import android.app.Application;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Pattern;

public class LauncherApplication extends Application {

  private static final String TAG = "LauncherApplication";

  private NameFilter mAppNameFilter = new NameFilter();

  public interface AppListener {
    void onRefreshApps();
  }

  private ArrayList<AppListener> mAppListeners = new ArrayList<>();

  @Override
  public void onCreate() {
    super.onCreate();
    loadAppNameFilters();
  }

  public void addAppListener(AppListener l) {
    mAppListeners.add(l);
  }

  public void removeAppListeners(AppListener l) {
    mAppListeners.remove(l);
  }

  public void refreshApps() {
    for (AppListener l : mAppListeners) {
      l.onRefreshApps();
    }
  }

  public NameFilter getAppPackageFilter() {
    return mAppNameFilter;
  }

  private void loadAppNameFilters() {
    try {
      loadRegexStrings(mAppNameFilter.whitelist, "whitelist.txt");
      loadRegexStrings(mAppNameFilter.blacklist, "blacklist.txt");
    } catch (IOException e) {
      Log.e(TAG, "error loading app name filter", e);
    }
  }

  private void loadRegexStrings(Collection<Pattern> dest, String assetFileName) throws IOException {
    String line;
    BufferedReader reader =
        new BufferedReader(new InputStreamReader(getAssets().open(assetFileName)));
    try {
      while ((line = reader.readLine()) != null) {
        Pattern regex = Pattern.compile(line);
        dest.add(regex);
      }
    } finally {
      reader.close();
    }
  }
}
