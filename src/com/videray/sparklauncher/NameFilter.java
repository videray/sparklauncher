/*
 *  (C) Copyright 2018-2019 Videray Technologies, Inc. (https://videray.com)
 *  All rights reserved.
 *
 */

package com.videray.sparklauncher;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NameFilter implements Predicate<String> {

  public final Set<Pattern> whitelist = new HashSet<>();
  public final Set<Pattern> blacklist = new HashSet<>();

  @Override
  public boolean test(String input) {
    for (Pattern p : whitelist) {
      Matcher m = p.matcher(input);
      if (m.find()) {
        return true;
      }
    }

    for (Pattern p : blacklist) {
      Matcher m = p.matcher(input);
      if (m.find()) {
        return false;
      }
    }

    return true;
  }
}
