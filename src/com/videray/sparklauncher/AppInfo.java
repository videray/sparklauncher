/*
 *  (C) Copyright 2018-2019 Videray Technologies, Inc. (https://videray.com)
 *  All rights reserved.
 *
 */

package com.videray.sparklauncher;

import android.graphics.drawable.Drawable;

public class AppInfo {

  public CharSequence label;
  public CharSequence packageName;
  public Drawable icon;
}
