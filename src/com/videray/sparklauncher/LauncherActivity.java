/*
 *  (C) Copyright 2018-2019 Videray Technologies, Inc. (https://videray.com)
 *  All rights reserved.
 *
 */

package com.videray.sparklauncher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class LauncherActivity extends Activity implements LauncherApplication.AppListener {

  public static final String MAIN_LAUNCH_PACKAGE = "com.videray.backscatter";

  private ArrayList<AppInfo> mAppList;
  private RecyclerView mAppListView;
  private AppLauncherAdapter mListViewAdapter;
  private ImageButton mMainLaunchButtion;
  private ImageButton mPowerButton;
  private final View.OnClickListener mOnPowerClick = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      Intent i = new Intent("com.android.internal.intent.action.REQUEST_SHUTDOWN");
      i.putExtra("android.intent.extra.KEY_CONFIRM", true);
      startActivity(i);
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_launcher);

    mMainLaunchButtion = findViewById(R.id.mainbutton);
    mMainLaunchButtion.setOnClickListener(mOnMainButtonClick);

    mPowerButton = findViewById(R.id.powerbutton);
    mPowerButton.setOnClickListener(mOnPowerClick);

    mAppListView = findViewById(R.id.applist);
    // mAppListView.setHasFixedSize(true);
    // LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    // layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
    GridLayoutManager layoutManager =
        new GridLayoutManager(this, 4, GridLayoutManager.VERTICAL, false);

    mAppListView.setLayoutManager(layoutManager);
    mListViewAdapter = new AppLauncherAdapter();
    mAppListView.setAdapter(mListViewAdapter);
  }

  private void reloadApps() {
    NameFilter namefilter = ((LauncherApplication) getApplication()).getAppPackageFilter();

    PackageManager manager = getPackageManager();
    mAppList = new ArrayList<AppInfo>();

    Intent i = new Intent(Intent.ACTION_MAIN, null);
    i.addCategory(Intent.CATEGORY_LAUNCHER);

    List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);
    for (ResolveInfo ri : availableActivities) {
      AppInfo app = new AppInfo();
      app.label = ri.loadLabel(manager);
      app.packageName = ri.activityInfo.packageName;
      app.icon = ri.activityInfo.loadIcon(manager);
      Log.d("APP", "app: " + app.packageName);
      if (namefilter.test(app.packageName.toString())) {
        mAppList.add(app);
      }
    }

    mListViewAdapter.notifyDataSetChanged();
  }

  @Override
  protected void onResume() {
    super.onResume();
    LauncherApplication app = (LauncherApplication) getApplication();
    app.addAppListener(this);
    reloadApps();
  }

  @Override
  protected void onPause() {
    LauncherApplication app = (LauncherApplication) getApplication();
    app.removeAppListeners(this);
    super.onPause();
  }

  private final View.OnClickListener mOnMainButtonClick =
      new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          Intent launchIntent = getPackageManager().getLaunchIntentForPackage(MAIN_LAUNCH_PACKAGE);
          if (launchIntent == null) {
            String text = getString(R.string.app_not_installed, MAIN_LAUNCH_PACKAGE);
            Toast.makeText(LauncherActivity.this, text, Toast.LENGTH_LONG).show();
          } else {
            startActivity(launchIntent);
          }
        }
      };

  @Override
  public void onRefreshApps() {
    reloadApps();
  }

  private static class AppInfoView extends RecyclerView.ViewHolder implements View.OnClickListener {

    static AppInfoView create(Context context) {
      View view = LayoutInflater.from(context).inflate(R.layout.item_applauncher, null, false);
      AppInfoView holder = new AppInfoView(view);
      holder.icon = view.findViewById(R.id.icon);
      holder.text = view.findViewById(R.id.name);
      return holder;
    }

    AppInfo item;
    ImageView icon;
    TextView text;

    AppInfoView(View view) {
      super(view);
      view.setOnClickListener(this);
    }

    void setAppInfo(AppInfo item) {
      this.item = item;
//      icon.setImageDrawable(item.icon);
      Drawable iconDrawable = item.icon;

      icon.setImageDrawable(iconDrawable);
      text.setText(item.label);
    }

    @Override
    public void onClick(View v) {
      Context ctx = v.getContext();
      Intent launchIntent =
          ctx.getPackageManager().getLaunchIntentForPackage(item.packageName.toString());
      ctx.startActivity(launchIntent);
    }
  }

  private class AppLauncherAdapter extends RecyclerView.Adapter<AppInfoView> {

    @NonNull
    @Override
    public AppInfoView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      return AppInfoView.create(getBaseContext());
    }

    @Override
    public void onBindViewHolder(@NonNull AppInfoView holder, int position) {
      AppInfo item = mAppList.get(position);
      holder.setAppInfo(item);
    }

    @Override
    public int getItemCount() {
      return mAppList.size();
    }
  }
}
