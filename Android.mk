LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)


LOCAL_MODULE_TAGS := optional

LOCAL_STATIC_JAVA_LIBRARIES := \
    android-common

LOCAL_STATIC_ANDROID_LIBRARIES := \
        android-support-core-ui \
        android-support-compat \
        android-support-v7-recyclerview \
        android-support-v4 \
        android-support-v13


LOCAL_SRC_FILES := $(call all-java-files-under, src) $(call all-renderscript-files-under, src)
LOCAL_SDK_VERSION := current

LOCAL_PACKAGE_NAME := SparkLauncher
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true

LOCAL_OVERRIDES_PACKAGES := Launcher2 Launcher3 Launcher3QuickStep

LOCAL_PROGUARD_FLAG_FILES := proguard.txt

LOCAL_USE_AAPT2 := true
LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/res

include $(BUILD_PACKAGE)


